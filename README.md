![A screenshot of the Scrythe application, showing the use of searching for text in a folder of images](media/screenshot.png)

Scrythe is a (wip) OCR application that allows you to search for text in a directory of images. Mostly just a proof-of-concept.

Written in Rust, using egui for UI and Tesseract for OCR.

Requires a folder called `images` in the project root (same folder as this readme) with various .png files.