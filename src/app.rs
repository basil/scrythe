use crate::util::{image::Image, get_images::get_images};

pub struct Scrythe {
    text: String,
    images: Vec<Image>,
}

impl Default for Scrythe {
    fn default() -> Self {
        Self {
			text: String::new(),
			images: vec![]
            // name: "Arthur".to_owned(),
            // age: 42,
        }
    }
}

impl eframe::App for Scrythe {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            // ui.heading("My egui Application");
            // ui.horizontal(|ui| {
            //     let name_label = ui.label("Your name: ");
            //     ui.text_edit_singleline(&mut self.name)
            //         .labelled_by(name_label.id);
            // });

			ui.add(egui::TextEdit::singleline(&mut self.text));

			if ui.button("Update").clicked() {
				println!("click!");

				match get_images(ui, "images") {
					Ok(images) => { self.images = images; },
					Err(err) => eprintln!("{}", err)
				};
			};

			ui.separator();
		
            ui.label(format!("Text: '{}'", self.text));

			egui::ScrollArea::vertical()
			.auto_shrink([false; 2])
			.show(ui, |ui| {
				// egui::Image
				ui.horizontal_wrapped(|ui| {
					ui.spacing_mut().item_spacing = egui::Vec2::splat(16.0);

					for image in &self.images {

						if !image.text.to_lowercase().contains(&self.text.to_lowercase()) { continue; }
						// let image = &image.texture;
						// let image = image.as_ref().expect("Couldn't get image");

						// ui.show(image.image);

						// Scaling issues
						// image.image.show_max_size(ui, egui::vec2(200.0, 200.0));
						
						// // Button
						// let button = egui::ImageButton::new(image.image.texture_id(ui.ctx()), egui::vec2(200.0, 200.0));
						// ui.add(button);

						// let path = image.path.to_str().unwrap();

						// Temporary solution
						let path = image.path.file_name().unwrap().to_str().unwrap();
						image.image
						.show_max_size(ui, egui::vec2(200.0, 200.0)).
						// on_hover_text(path);
						on_hover_text(&image.text);

						// Scaling issues
						// egui::Frame::none()
						// .fill(egui::Color32::RED)
						// .show(ui, |ui| {
						// 	image.image.show_max_size(ui, egui::vec2(200.0, 200.0)).on_hover_text("aaa");
						// 	// ui.label("Label with red background");
						// });

						// .on_hover_ui(tooltip_ui).clicked() {
						// 	// 			ui.output_mut(|o| o.copied_text = chr.to_string());
						// 	// 		}

						
						// ui.image(image, image.size_vec2());
						// image.show(ui);
					}

					// for (&chr, name) in named_chars {
					// 	if filter.is_empty() || name.contains(filter) || *filter == chr.to_string() {
					// 		let button = egui::Button::new(
					// 			egui::RichText::new(chr.to_string()).font(self.font_id.clone()),
					// 		)
					// 		.frame(false);
	
					// 		let tooltip_ui = |ui: &mut egui::Ui| {
					// 			ui.label(
					// 				egui::RichText::new(chr.to_string()).font(self.font_id.clone()),
					// 			);
					// 			ui.label(format!("{}\nU+{:X}\n\nClick to copy", name, chr as u32));
					// 		};
	
					// 		if ui.add(button).on_hover_ui(tooltip_ui).clicked() {
					// 			ui.output_mut(|o| o.copied_text = chr.to_string());
					// 		}
					// 	}
					// }
				});
			});

			// let texture: &egui::TextureHandle = self.texture.get_or_insert_with(|| {
			// 	// Load the texture only once.
			// 	ui.ctx().load_texture(
			// 		"my-image",
			// 		egui::ColorImage::example(),
			// 		Default::default()
			// 	)
			// });
			// ui.image(texture, texture.size_vec2());

			// egui::ScrollArea::vertical().show(ui, |ui| {
			// 	// egui::Image
			// 	ui.horizontal_wrapped(|ui| {
			// 		ui.spacing_mut().item_spacing = egui::Vec2::splat(2.0);
	
			// 		for (&chr, name) in named_chars {
			// 			if filter.is_empty() || name.contains(filter) || *filter == chr.to_string() {
			// 				let button = egui::Button::new(
			// 					egui::RichText::new(chr.to_string()).font(self.font_id.clone()),
			// 				)
			// 				.frame(false);
	
			// 				let tooltip_ui = |ui: &mut egui::Ui| {
			// 					ui.label(
			// 						egui::RichText::new(chr.to_string()).font(self.font_id.clone()),
			// 					);
			// 					ui.label(format!("{}\nU+{:X}\n\nClick to copy", name, chr as u32));
			// 				};
	
			// 				if ui.add(button).on_hover_ui(tooltip_ui).clicked() {
			// 					ui.output_mut(|o| o.copied_text = chr.to_string());
			// 				}
			// 			}
			// 		}
			// 	});
			// });

            // ui.add(egui::Slider::new(&mut self.age, 0..=120).text("age"));
            // if ui.button("Click each year").clicked() {
            //     self.age += 1;
            // }
            // ui.label(format!("Hello '{}', age {}", self.name, self.age));
        });
    }
}

