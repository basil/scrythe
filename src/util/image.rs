use std::{fs::File, io::Read, error::Error, path::PathBuf};

use egui_extras::RetainedImage;

pub struct Image {
	pub path: PathBuf,
	pub image: RetainedImage,
	pub text: String,
	// pub texture: Option<egui::TextureHandle>,
}

impl Image {
    pub fn from_path(ui: &mut egui::Ui, path: PathBuf) -> Result<Self, Box<dyn Error>> {
		println!("Loading {}", path.to_str().unwrap());

		// egui::ColorImage::
		// let texture = ui.ctx().load_texture(
		// 	"my-image",
		// 	egui::ColorImage::example(),
		// 	Default::default()
		// );
		
		// ui.image(texture, texture.size_vec2());

		// Load texture
		let name = path.clone();
		let name = name.to_str().unwrap();
		let mut buffer = vec![];
		File::open(&path)?.read_to_end(&mut buffer)?;
		let image = RetainedImage::from_image_bytes(name, &buffer[..])?;

		// let Ok(text)
		let text = tesseract::ocr(name, "eng")?;
		
		//  {
		// 	Ok(str) => println!("{str}"),
		// 	Err(_) => panic!("aaaa! fail!!!!!!!")
		// };

		// File::open (filename)?.read_to_end (&mut buffer)?;
		// let image = RetainedImage::from_image_bytes(debug_name, image_bytes)?;

        Ok(Self {
			path,
			image,
			text
			// texture: Some(texture)
			// text: String::new()
        })
    }

	// pub fn show(&self, ui: &mut egui::Ui) {
	// 	let texture = self.texture.expect("Couldn't get Image texture");
	// 	// let image = &image.texture;
	// 	// let image = image.as_ref().expect("Couldn't get image");

	// 	ui.image(&texture, texture.size_vec2());
	// }

	// pub fn texture(self: Self) -> &egui::TextureHandle {
	// 	&self.texture.expect("Couldn't get thing")
	// }
}
