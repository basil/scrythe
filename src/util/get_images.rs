use std::path::PathBuf;
use super::image::Image;

pub fn get_images(ui: &mut egui::Ui, folder: &str) -> Result<Vec<Image>, &'static str> {
	let folder = PathBuf::from(folder);

	if !folder.is_dir() {
		return Err("Path is not a folder")
	}

	let mut images: Vec<Image> = Vec::new();

	let Ok(folder) = folder.read_dir() else { return Err("Couldn't read folder") };

	for path in folder {
		let Ok(path) = path else {
			todo!("Handle image failing");
		};

		let path = path.path();

		let is_file = path.is_file();
		// TODO: Check image extensions/types

		// folder.push("Pasted image 20230412123300.png");
		// folder
		if !is_file { continue };

		let image = Image::from_path(ui, path).unwrap();

		images.push(image);
		// path.metadata().unwrap()

		// if path.
	}

	// folder.push("Pasted image 20230412123300.png");
	// folder.to_string_lossy()

	Ok(images)
	// Ok(vec![
	// 	Image::from_path(ui, folder.to_str().unwrap()).expect("nya :3")
	// ])
}